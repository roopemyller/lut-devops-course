const express = require('express')
const app = express()
const port = process.env.PORT || 3000

app.get('/', (req, res) => {
    res.send('THIS IS A NEW CHANGE!')
})

app.get('/feature1', (req, res) => {
    res.send('feature1')
})

app.get('/feature2', (req, res) => {
    res.send('feature2')
})

module.exports = app;

